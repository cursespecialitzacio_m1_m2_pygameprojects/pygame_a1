# Projecte de Joc amb Pygame - README

Aquest és un projecte de joc desenvolupat amb Pygame, un joc del gènere "Naus" inspirat en el joc clàssic Ikaruga. El joc presenta una mecànica principal en què el jugador pot canviar el color de la seva nau de blanc a negre (o viceversa) amb un sol botó. Aquesta elecció de color afecta la capacitat dels projectils del jugador per fer mal als enemics, ja que només poden fer mal als enemics del mateix color. Els enemics també tenen colors fixos i només poden ser ferits per projectils del color oposat.

## Desenvolupador
- **Nom**: Nohadon (Pol Gonzalo)

## Enllaç al Projecte
[Enllaç al repositori del projecte](https://gitlab.com/cursespecialitzacio_m1_m2_pygameprojects/pygame_a1)

## Característiques del Joc

### Gènere
- Naus

### Mecànica Principal
- Inspirat en Ikaruga, el jugador pot canviar el color de la seva nau entre blanc i negre amb un botó.
- Els projectils del jugador només fan mal als enemics del mateix color.
- Els enemics també tenen colors fixos (blanc o negre) i només poden ser ferits per projectils del color oposat.
- El jugador acumula punts cada vegada que impacta un projectil enemic i té un límit. Quan arriba a aquest límit, pot llançar un raig més gran que causa més mal.

### Moviment
- Moviment en l'eix X i Y.
- Sense rotacions.

### Comportament dels Enemics
#### Enemics Normals
- Apareixen des de la part superior amb formacions específiques, com quadrats 3x3 amb files alternades de blanc i negre.
- Xocar amb un enemic resta vida al jugador, llevat que l'enemic sigui del mateix color que el jugador.
- Els enemics disparen projectils cada 3 a 5 segons (amb temporitzadors aleatoris).

#### Cap de la Banda
- Apareix des de la part superior com un enemic més gran.
- Es mou entre diverses posicions intentant arribar al jugador en l'eix X.
- Posseeix 2 tipus de raigs (no projectils).
- Si el raig del cap de la banda impacta al jugador, aquest mor instantàniament.

### Comportament dels Projectils
- Els projectils tenen informació sobre el seu color quan s'instàncien.
- A cada fotograma (frame) comprova si ha col·lisionat amb algun objecte i determina el tipus d'objecte amb què col·lisiona.

#### Projectils del Jugador
- Si col·lisionen amb un enemic:
  - Es compara el color del projectil amb el de l'enemic.
  - Si són del mateix color, el projectil destrueix l'enemic.
  - Si no, el projectil continua el seu moviment.

#### Projectils dels Enemics
- Si col·lisionen amb el jugador:
  - Es compara el color del projectil amb el del jugador.
  - Si són del mateix color, el projectil destrueix el jugador.
  - Si no, el projectil continua el seu moviment.

### Pantalles del Joc
- El joc consta de dues pantalles: el nivell normal i corrent, i una segona pantalla que presenta el cap de la banda (Boss).

## Punts Adicionals
- Encara que no està 'displejat' té un sistema de guardat amb el score del jugador
- El sistema està montat de forma molt escalable i molt fàcilment es pot crear més nivells de forma més sencilla gràcies al EnemyHandler
- Tot està molt optimitzat a partir de patrons de disseny com Observers
- El codi està organitzat, net i a més està separat per diferents arxius segons la funcionalitat de la classe
- S'ha utilitzat POO (Programació Orientada a Objectes)

# Pygame Game Project - README

This is a game project developed with Pygame, a "Spaceships" genre game inspired by the classic game Ikaruga. The game features a core mechanic where the player can switch the color of their ship from white to black (or vice versa) with a single button. This color choice affects the player's projectiles' ability to damage enemies, as they can only harm enemies of the same color. The enemies also have fixed colors and can only be damaged by projectiles of the opposite color.

## Developer
- **Name**: Nohadon (Pol Gonzalo)

## Project Link
[Link to the project repository](https://gitlab.com/cursespecialitzacio_m1_m2_pygameprojects/pygame_a1)

## Game Features

### Genre
- Spaceships

### Core Mechanic
- Inspired by Ikaruga, the player can switch their ship's color between white and black with a button.
- Player's projectiles only damage enemies of the same color.
- Enemies also have fixed colors (white or black) and can only be hurt by projectiles of the opposite color.
- The player accumulates points each time an enemy projectile hits and has a limit. When reaching this limit, they can launch a larger beam causing more damage.

### Movement
- Movement on the X and Y axes.
- No rotations.

### Enemy Behavior
#### Normal Enemies
- Appear from the top in specific formations, like 3x3 squares with alternating rows of white and black.
- Colliding with an enemy reduces the player's health unless the enemy is of the same color as the player.
- Enemies shoot projectiles every 3 to 5 seconds (with random timers).

#### Boss
- Appears from the top as a larger enemy.
- Moves between various positions attempting to reach the player on the X axis.
- Possesses 2 types of beams (non-projectiles).
- If the boss's beam hits the player, the player dies instantly.

### Projectile Behavior
- Projectiles have information about their color when instantiated.
- Each frame checks if they've collided with any object and determines the type of object they collide with.

#### Player's Projectiles
- If they collide with an enemy:
  - Compares the projectile's color with the enemy's color.
  - If they're the same color, the projectile destroys the enemy.
  - If not, the projectile continues its movement.

#### Enemy Projectiles
- If they collide with the player:
  - Compares the projectile's color with the player's color.
  - If they're the same color, the projectile destroys the player.
  - If not, the projectile continues its movement.

### Game Screens
- The game consists of two screens: the regular and current level and a second screen featuring the Boss.
