####################################### I M P O R T S #######################################
import pygame as pygame
from ServicesHandlersAndConstants.Constants import GameConstants

####################################### C L A S S E S #######################################
class GameMenu():
    def __init__(self, screen : pygame.Surface) -> None:
        self.SCREEN = screen
        self.BackgroundI = pygame.image.load(GameConstants.PATH_BACKGROUND_MENU)
        self.TitleI = pygame.image.load(GameConstants.PATH_TITLE_MENU)
        self.Start_BI = pygame.image.load(GameConstants.PATH_START_BUTT_MENU)
        self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_MENU)


    def Update(self, mouseInput : tuple[bool, bool, bool]) -> bool:
        res = None
        self.SCREEN.blit(self.BackgroundI, self.BackgroundI.get_rect())
        self.SCREEN.blit(self.TitleI, self.TitleI.get_rect().move(250,300))
        if self.Start_BI.get_rect().move(1200, 810).collidepoint(pygame.mouse.get_pos()):
            if mouseInput[0]:
                self.Start_BI = pygame.image.load(GameConstants.PATH_START_BUTT_PRESS_MENU)
                res = True
            else: 
                self.Start_BI = pygame.image.load(GameConstants.PATH_START_BUTT_HOVER_MENU)
        else:
            self.Start_BI = pygame.image.load(GameConstants.PATH_START_BUTT_MENU)
        
        if self.Exit_BI.get_rect().move(300, 810).collidepoint(pygame.mouse.get_pos()):
            if mouseInput[0]:
                self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_PRESS_MENU)
                res = False
            else: 
                self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_HOVER_MENU)
        else:
            self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_MENU)


        self.SCREEN.blit(self.Start_BI, self.Start_BI.get_rect().move(1200, 810))
        self.SCREEN.blit(self.Exit_BI, self.Exit_BI.get_rect().move(300,810))
        pygame.display.flip()
        return res
    
class DeathMenu():
    def __init__(self, screen : pygame.Surface) -> None:
        self.SCREEN = screen
        self.TitleI = pygame.image.load(GameConstants.PATH_TITLE_DEATH_MENU)
        self.BackMenu_BI = pygame.image.load(GameConstants.PATH_BACKMENU_BUTT_DEATH)
        self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_MENU)
    
    def Update(self, mouseInput : tuple[bool, bool, bool]) -> bool:
            res = None
            self.SCREEN.blit(self.TitleI, self.TitleI.get_rect().move(450,300))
            if self.BackMenu_BI.get_rect().move(1200, 600).collidepoint(pygame.mouse.get_pos()):
                if mouseInput[0]:
                    self.BackMenu_BI = pygame.image.load(GameConstants.PATH_BACKMENU_BUTT_PRESS_DEATH)
                    res = True
                else: 
                    self.BackMenu_BI = pygame.image.load(GameConstants.PATH_BACKMENU_BUTT_HOVER_DEATH)
            else:
                self.BackMenu_BI = pygame.image.load(GameConstants.PATH_BACKMENU_BUTT_DEATH)
            
            if self.Exit_BI.get_rect().move(300, 600).collidepoint(pygame.mouse.get_pos()):
                if mouseInput[0]:
                    self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_PRESS_MENU)
                    res = False
                else: 
                    self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_HOVER_MENU)
            else:
                self.Exit_BI = pygame.image.load(GameConstants.PATH_EXIT_BUTT_MENU)


            self.SCREEN.blit(self.BackMenu_BI, self.BackMenu_BI.get_rect().move(1200, 600))
            self.SCREEN.blit(self.Exit_BI, self.Exit_BI.get_rect().move(300,600))
            pygame.display.flip()
            return res