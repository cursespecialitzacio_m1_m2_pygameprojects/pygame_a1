from enum import Enum

####################################### E N U M S #######################################
class GameStates(Enum):
    PRINCIPAL_MENU = 0,
    LVL_1 = 1,
    BOSS = 2,
    DEATH = 3,
    EXIT = -1

class Color(Enum):
    BLUE = 0
    RED = 1

class MissileOwner(Enum):
    PLAYER = 0,
    ENEMY = 1,
    BOSS = 2

class TypeEnemyShooter(Enum):
    ONE_PROJECTILE = 0,
    TWO_PROJECTILE = 1,
    THREE_PROJECTILE = 2