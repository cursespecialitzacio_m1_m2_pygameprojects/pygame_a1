####################################### I M P O R T S #######################################
import random as rnd

from .Player import Player
from .SpaceShip import SpaceShipEnemies
from .Missile import Missile
from .GameEnums import Color, TypeEnemyShooter
from .EnemyPrefab import EnemyPrefabs, EnemyPrefabStruct
####################################### T Y P E S #######################################
CollisionInformation = tuple[bool, int]

####################################### F A T H E R  C L A S S E S #######################################
class SpawnerEnemiesManager():
    enemy_spaceships : list[SpaceShipEnemies] = []
    TIME_BEFORE_WAVES = 5
    INCREMENTAL_SCALE_NUM_ENEMIES = 2
    LIMIT_SCREEN_DESPAWN = 1080
    def __init__(self, MainPlayer : Player, Waves = 1) -> None:
        self.Player = MainPlayer
        self.Waves = Waves
        self.currentCooldown = 0
    
    def spawnCheckExec(self, current_delta):
        if len(self.enemy_spaceships) == 0:
            self.currentCooldown+=current_delta
            if (self.currentCooldown / 1000) >= self.TIME_BEFORE_WAVES:
                self.currentCooldown = 0
                numOfEnemies = self.Waves * self.INCREMENTAL_SCALE_NUM_ENEMIES
                self.Waves += 1
                for x in range(numOfEnemies):
                    rndint = rnd.randint(0,1)
                    if rndint == 0:
                        color = Color.BLUE
                    else:
                        color = Color.RED
                    struct = EnemyPrefabs.PREFABS_LIST[rnd.randint(0, len(EnemyPrefabs.PREFABS_LIST)-1)]
                    self.enemy_spaceships.append(SpaceShipEnemies(rnd.randrange(100, 1800), -50, struct.Velocity, color, struct.FrequencyShoot, 0.1, struct.TypeShoot, struct.Points, struct.Energy, struct.healAtDie))

    def movementEnemies(self, screen, delta):
        outOfRangeSpaceshipts = []
        for x in self.enemy_spaceships:
            x.Move(delta, screen)
            if x.y > self.LIMIT_SCREEN_DESPAWN:
                outOfRangeSpaceshipts.append(x)
                self.Player.score-=5
                self.Player.score = 0 if self.Player.score < 0 else self.Player.score
        for e in outOfRangeSpaceshipts:
            self.enemy_spaceships.remove(e)
    
    def checkShootEnemies(self, delta) -> list[Missile]:
        arr = []
        for x in self.enemy_spaceships:
            if x.checkIfShoot(delta):
                arr+=x.shoot()
        return arr
    
    def checkCollisionEnemies(self, listOfMissiles : list[Missile]):
        for x in self.enemy_spaceships:
            res : CollisionInformation = x.checkIfCollision(listOfMissiles)
            if res[0]:
                self.enemy_spaceships.remove(x)
                if res[1]!=-1:
                    del listOfMissiles[res[1]]
                    self.Player.onEnemyDeath(x.score, x.energy, x.healPlayer)

    def resetValues(self):
        self.enemy_spaceships.clear()
        self.Waves = 1
        self.currentCooldown = 0
                
        
        