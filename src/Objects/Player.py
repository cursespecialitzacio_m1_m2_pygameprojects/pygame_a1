####################################### I M P O R T S #######################################
import random as rnd

class Player():
    MEGA_RAY_COST = 20
    MAX_ENERGY_CAPACITY = 40
    MAX_HEALTH = 10
    def __init__(self, name="Player1", score=0, health = 10):
        self.name=name
        self.score=score
        self.Energy=0
        self.Health = health

    def __str__(self) -> str:
        return "[ Player (Name:"+self.name+", score:"+str(self.score)+", Health:"+str(self.Health)+") ]"

    def onEnemyDeath(self, Points : int, Energy : int, Health : bool) -> None :
        self.score += Points
        self.Energy += 5
        self.Energy = self.MAX_ENERGY_CAPACITY if(self.Energy > self.MAX_ENERGY_CAPACITY) else self.Energy
        if Health:
            self.Health += rnd.randint(1,5)
            self.Health = self.MAX_HEALTH if self.Health > self.MAX_HEALTH else self.Health

    def useMegaProjectile(self) -> bool:
        if self.Energy >= self.MEGA_RAY_COST:
            self.Energy-=self.MEGA_RAY_COST
            return True
        return False
    
    def HIT(self):
        self.Health-=1
        return self.Health <= 0
