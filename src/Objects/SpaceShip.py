####################################### I M P O R T S #######################################
import sys
import random as rnd
sys.path.append("..") 
from abc import ABC, abstractmethod
from .GameEnums import MissileOwner, Color, TypeEnemyShooter
from .Player import Player
from .Missile import Missile
from ServicesHandlersAndConstants.Constants import GameConstants
from ServicesHandlersAndConstants.GameFilesManager import GameFilesManager
import pygame as pygame
####################################### I N I T  S E R V I C E #######################################
GFM = GameFilesManager()

####################################### T Y P E S #######################################
CollisionInformation = tuple[bool, int]

####################################### F A T H E R  C L A S S E S #######################################
class SpaceShip(ABC):
    def __init__(self, x=910, y=505, velocity=0.3, color=Color.BLUE):
        self.x = x
        self.y = y
        self.velocity = velocity
        self.color = color
    
    def __str__(self) -> str:
        return "[ Spaceship (x:"+str(self.x)+", y:"+str(self.y)+", velocity:"+str(self.velocity)+", color:"+str(self.color)+") ]"
    
    @abstractmethod
    def shoot():
        pass


####################################### S O N  C L A S S E S #######################################
class SpaceShipPlayer(SpaceShip):
    def __init__(self, x=910, y=505, velocity=0.6, color=Color.BLUE, player = Player()):
        super().__init__(x, y, velocity, color)
        self.player = player
        path = GFM.getActualProjectFolder()
        self.image = pygame.image.load( path + GameConstants.PATH_PLAYER_SPACESHIP_B )
        self.colour_trggr = False
        self.shoot_trggr = False
        self.mshoot_trggr = False

    def __str__(self) -> str:
        tmp = super().__str__()
        return "[ Player Spaceship (Spaceship:"+tmp+", Player:"+self.player.__str__()+") ]"
    
    def movePlayer(self, x, y, screen, delta):
        new_x1 = (self.x + (x * self.velocity * delta)) - GameConstants.P_I_W
        new_y1 = (self.y + (y * self.velocity * delta)) - GameConstants.P_I_H
        new_x2 = (self.x + (x * self.velocity * delta)) + GameConstants.P_I_W
        new_y2 = (self.y + (y * self.velocity * delta)) + GameConstants.P_I_H
        
        self.x = self.x if (new_x1 < 0 or new_x2 > screen.get_size()[0]) else (self.x + (x * self.velocity * delta))
        self.y = self.y if (new_y1 < 0 or new_y2 > screen.get_size()[1]) else (self.y + (y * self.velocity * delta))
        
        spaceShipGameObject = self.image.get_rect().move(self.x - GameConstants.P_I_W, self.y - GameConstants.P_I_H)
        screen.blit(self.image, spaceShipGameObject)
    
    def changeColor(self, pressed):
        if self.colour_trggr and pressed:
            pass
        elif self.colour_trggr and pressed==False:
            self.colour_trggr = False
        elif self.colour_trggr == False and pressed:
            path = GFM.getActualProjectFolder()
            if self.color==Color.BLUE:
                self.color=Color.RED
                self.image=pygame.image.load( path + GameConstants.PATH_PLAYER_SPACESHIP_R )
            else:
                self.color=Color.BLUE
                self.image=pygame.image.load( path + GameConstants.PATH_PLAYER_SPACESHIP_B )
            self.colour_trggr = True
    
    def shoot(self, trigger, mega) -> Missile:
        if mega:
            if self.shoot_trggr and trigger :
                return None
            elif self.shoot_trggr and trigger == False:
                self.shoot_trggr = False
                return None
            elif self.shoot_trggr == False and trigger and self.player.useMegaProjectile():
                self.shoot_trggr = True
                return Missile(MissileOwner.PLAYER, self.color, 0.7, self.x, self.y-GameConstants.P_I_H, 0, -1, mega,500000)
        else:
            if self.mshoot_trggr and trigger :
                return None
            elif self.mshoot_trggr and trigger == False:
                self.mshoot_trggr = False
                return None
            elif self.mshoot_trggr == False and trigger:
                self.mshoot_trggr = True
                return Missile(MissileOwner.PLAYER, self.color, 0.7, self.x, self.y-GameConstants.P_I_H, 0, -1, mega,500000)
        
    def checkCollision(self, missiles : list[Missile]) -> bool:
        spriteOwn = self.image.get_rect().move(self.x-GameConstants.P_I_W, self.y-GameConstants.P_I_H)
        index = -1
        for x in missiles:
            spriteMis = x.image.get_rect().move(x.position.x - GameConstants.M_W, x.position.y - GameConstants.M_H)            
            if (spriteOwn.colliderect(spriteMis) and x.Owner == MissileOwner.ENEMY and x.Color != self.color):
                index = missiles.index(x)
        if index != -1:
            del missiles[index]
            return self.player.HIT()
        else:
            return False
    
    def resetValues(self) :
        self.player.Energy=0
        self.player.Health=self.player.MAX_HEALTH
        self.player.score=0
        self.player.name="Player1"
        self.image = pygame.image.load(GameConstants.PATH_PLAYER_SPACESHIP_B)
        self.color = Color.BLUE
        self.x=910
        self.y=505


        

class SpaceShipEnemies(SpaceShip):
    RADIOUS = 100
    def __init__(self, x=910, y=505, velocity=0.3, color=Color.BLUE, CoolDownShoot = 2.5, velocity_axis_x = 0.2, typeEnemyShooter = TypeEnemyShooter.ONE_PROJECTILE, score : int = 0, energy : int = 0, healPlayer : bool = False):
        super().__init__(x, y, velocity, color)
        self.init_x = x
        self.typeOfShoot = typeEnemyShooter
        self.cooldownBetweenShots = CoolDownShoot
        self.currentDir =  rnd.randint(0,1)
        self.velocityInXAxis = velocity_axis_x if self.currentDir == 1 else (velocity_axis_x * -1)
        self.image = pygame.image.load(GameConstants.PATH_ENEMY_SPACESHIP_B) if self.color == Color.BLUE else pygame.image.load(GameConstants.PATH_ENEMY_SPACESHIP_R)
        self.currentCooldown = 0
        self.score = score
        self.energy = energy
        self.healPlayer = healPlayer
    
    def Move(self, delta, screen):
        if (self.x <= (self.init_x - self.RADIOUS)) and self.currentDir==0:
            self.currentDir=1
            self.velocityInXAxis = (self.velocityInXAxis * -1)
        elif (self.x >= (self.init_x + self.RADIOUS)) and self.currentDir==1:
            self.currentDir=0
            self.velocityInXAxis = (self.velocityInXAxis * -1)

        self.x = self.x + self.velocityInXAxis * delta
        self.y = self.y + self.velocity * delta
        
        spaceShipGameObject = self.image.get_rect().move(self.x - GameConstants.E_I_W, self.y - GameConstants.E_I_H)
        screen.blit(self.image, spaceShipGameObject)

    def shoot(self) -> [Missile]:

        arrMissiles = []
        if self.typeOfShoot == TypeEnemyShooter.ONE_PROJECTILE:
            arrMissiles.append(Missile(MissileOwner.ENEMY, self.color, 0.3, self.x, self.y-GameConstants.E_I_H, 0, 1, False, 500000))
        elif self.typeOfShoot == TypeEnemyShooter.TWO_PROJECTILE:
            arrMissiles.append(Missile(MissileOwner.ENEMY, self.color, 0.6, self.x, self.y-GameConstants.E_I_H, 1, 1, False, 500000))
            arrMissiles.append(Missile(MissileOwner.ENEMY, self.color, 0.6, self.x, self.y-GameConstants.E_I_H, -1, 1, False, 500000))
        elif self.typeOfShoot == TypeEnemyShooter.THREE_PROJECTILE:
            arrMissiles.append(Missile(MissileOwner.ENEMY, self.color, 0.6, self.x, self.y-GameConstants.E_I_H, 0, 1, False, 500000))
            arrMissiles.append(Missile(MissileOwner.ENEMY, self.color, 0.6, self.x, self.y-GameConstants.E_I_H, 1, 1, False, 500000))
            arrMissiles.append(Missile(MissileOwner.ENEMY, self.color, 0.6, self.x, self.y-GameConstants.E_I_H, -1, 1, False, 500000))
        
        return arrMissiles
    
    def checkIfCollision(self, Missiles: list[Missile]) -> CollisionInformation:
        spriteOwn = self.image.get_rect().move(self.x-GameConstants.E_I_W, self.y-GameConstants.E_I_H)

        for x in Missiles:
            if x.Mega:
                spriteMis = x.image.get_rect().move(x.position.x - GameConstants.MM_S, x.position.y - GameConstants.MM_S)
            else:
                spriteMis = x.image.get_rect().move(x.position.x - GameConstants.M_W, x.position.y - GameConstants.M_H)  
                      
            if (spriteOwn.colliderect(spriteMis) and x.Owner == MissileOwner.PLAYER and x.Color == self.color and not x.Mega):
                return (True, Missiles.index(x))
            elif x.Mega and spriteOwn.colliderect(spriteMis):
                return (True, -1)

        return (False, -1)
    
    def checkIfShoot(self, current_delta) -> bool:
        self.currentCooldown+=current_delta
        if self.cooldownBetweenShots <= (self.currentCooldown / 1000):
            self.currentCooldown = 0
            return True
        return False