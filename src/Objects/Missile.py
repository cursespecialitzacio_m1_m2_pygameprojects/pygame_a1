####################################### I M P O R T S #######################################
import pygame as pygame 
from ServicesHandlersAndConstants.Constants import GameConstants
from ServicesHandlersAndConstants.GameFilesManager import GameFilesManager
from .GameEnums import Color, MissileOwner
####################################### I N I T  S E R V I C E #######################################
GFM = GameFilesManager()

####################################### F A T H E R  C L A S S E S #######################################

class Missile():
    def __init__(self, Owner, color, Velocity, x_pos, y_pos, x_dir, y_dir, mega : bool = False, timeOut=9999):
        self.Owner = Owner
        self.Color = color
        self.Velocity = Velocity
        self.position = pygame.Vector2( x_pos, y_pos )
        self.direction = pygame.Vector2( x_dir, y_dir )
        self.timeOut = timeOut
        self.Mega = mega
        path = GFM.getActualProjectFolder()
        if self.Owner == MissileOwner.PLAYER:
            if mega:
                self.image = pygame.image.load( path + GameConstants.PATH_MEGAMISSILE)
                self.Velocity+=0.1
            else:
                if color == Color.BLUE:
                    self.image = pygame.image.load( path + GameConstants.PATH_MISSILE_B )
                else:
                    self.image = pygame.image.load( path + GameConstants.PATH_MISSILE_R )
        elif self.Owner == MissileOwner.ENEMY:
            if color == Color.BLUE:
                self.image = pygame.image.load( path + GameConstants.PATH_MISSILE_B_E )
            else:
                self.image = pygame.image.load( path + GameConstants.PATH_MISSILE_R_E )
    
    def Update(self, screen, delta) -> int:
        if self.Mega:
            new_x1 = (self.position.x + (self.direction.x * self.Velocity * delta)) - GameConstants.MM_S
            new_y1 = (self.position.y + (self.direction.y * self.Velocity * delta)) - GameConstants.MM_S
            new_x2 = (self.position.x + (self.direction.x * self.Velocity * delta)) + GameConstants.MM_S
            new_y2 = (self.position.y + (self.direction.y * self.Velocity * delta)) + GameConstants.MM_S
        else:
            new_x1 = (self.position.x + (self.direction.x * self.Velocity * delta)) - GameConstants.M_W
            new_y1 = (self.position.y + (self.direction.y * self.Velocity * delta)) - GameConstants.M_H
            new_x2 = (self.position.x + (self.direction.x * self.Velocity * delta)) + GameConstants.M_W
            new_y2 = (self.position.y + (self.direction.y * self.Velocity * delta)) + GameConstants.M_H

        if(new_x1 > screen.get_size()[0] or new_y1 > screen.get_size()[1] or new_x2 < 0 or new_y2 < 0):
            return 1
        else:
            self.position.x = self.position.x + (self.direction.x * self.Velocity * delta)
            self.position.y = self.position.y + (self.direction.y * self.Velocity * delta)
        
        if self.Mega:
            MissileObject = self.image.get_rect().move(self.position.x - GameConstants.MM_S, self.position.y - GameConstants.MM_S)
        else:
            MissileObject = self.image.get_rect().move(self.position.x - GameConstants.M_W, self.position.y - GameConstants.M_H)
        screen.blit(self.image, MissileObject)
        return 0
    
    def __str__(self) -> str:
        return "[ Missile ( Owner: "+str(self.Owner)+", Color: "+str(self.Color)+", Velocity: "+str(self.Velocity)+", Position: "+str(self.position)+", Direction: "+str(self.direction)+", TimeOut: "+str(self.timeOut)+" ) ]"
