####################################### I M P O R T S #######################################
from dataclasses import dataclass
from .GameEnums import TypeEnemyShooter

####################################### E N E M Y  P R E F A B S  S T R U C T S #######################################
@dataclass
class EnemyPrefabStruct:
    TypeShoot : TypeEnemyShooter
    Velocity : float
    FrequencyShoot : float
    Points : int
    Energy : int
    healAtDie : bool

class EnemyPrefabs:
    PREFABS_LIST = [
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.05, 1.5, 2, 1, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.07, 1.2, 5, 3, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.1, 1, 7, 5, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.05, 1.5, 2, 1, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.07, 1.2, 5, 3, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.1, 1, 7, 5, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.05, 1.5, 2, 1, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.07, 1.2, 5, 3, False),
        EnemyPrefabStruct(TypeEnemyShooter.ONE_PROJECTILE, 0.1, 1, 7, 5, False),
        EnemyPrefabStruct(TypeEnemyShooter.TWO_PROJECTILE, 0.05, 1.5, 4, 3, False),
        EnemyPrefabStruct(TypeEnemyShooter.TWO_PROJECTILE, 0.07, 1.2, 6, 5, False),
        EnemyPrefabStruct(TypeEnemyShooter.TWO_PROJECTILE, 0.1, 1, 8, 8, False),
        EnemyPrefabStruct(TypeEnemyShooter.TWO_PROJECTILE, 0.05, 1.5, 4, 3, True),
        EnemyPrefabStruct(TypeEnemyShooter.TWO_PROJECTILE, 0.07, 1.2, 6, 5, True),
        EnemyPrefabStruct(TypeEnemyShooter.TWO_PROJECTILE, 0.1, 1, 8, 8, True),
        EnemyPrefabStruct(TypeEnemyShooter.THREE_PROJECTILE, 0.05, 1.5, 6, 5, True),
        EnemyPrefabStruct(TypeEnemyShooter.THREE_PROJECTILE, 0.07, 1.2, 10, 8, True),
        EnemyPrefabStruct(TypeEnemyShooter.THREE_PROJECTILE, 0.1, 1, 13, 10, True),
    ]