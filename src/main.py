####################################### M A D E  B Y  N O H A D O N  (Pol Gonzalo) #######################################

####################################### I M P O R T S #######################################
import pygame as pygame ## PYGAME

from Objects.SpaceShip import SpaceShipPlayer
from Objects.SpawnerEnemiesManager import SpawnerEnemiesManager
from Objects.Missile import Missile
from Objects.UI.GameMenu import GameMenu, DeathMenu

from ServicesHandlersAndConstants.Constants import GameConstants 
from ServicesHandlersAndConstants.GameFilesManager import GameFilesManager

from Objects.GameEnums import GameStates

####################################### I N I T  S E R V I C E S #######################################
GFM = GameFilesManager()

####################################### I N I T  C O N F I G U R A T I O N  P Y G A M E  &  V A R I A B L E S #######################################
pygame.init()

SCREEN_OF_GAME = pygame.display.set_mode([1920, 1000])

game_icon = pygame.image.load(GFM.getActualProjectFolder() + 'Resources/Images/icon.png')
pygame.display.set_caption(GameConstants.TITLE_OF_GAME)
pygame.display.set_icon(game_icon)
clock = pygame.time.Clock()

isPlaying = True

PRINCIPAL_MENU : GameMenu = GameMenu(SCREEN_OF_GAME)          ## Principal menu
PLAYER_SPACE_SHIP : SpaceShipPlayer = SpaceShipPlayer()       ## Player Init
SEM = SpawnerEnemiesManager(PLAYER_SPACE_SHIP.player, 1)      ## Enemies Handler Init
DEATH_MENU : DeathMenu = DeathMenu(SCREEN_OF_GAME)            ## Death menu

Missiles_List : list[Missile] = []
CurrentGameState : GameStates = GameStates.PRINCIPAL_MENU

####################################### P R I N C I P A L  F U N C T I O N S #######################################
def inputPlayer(InputSet, MouseInputSet, current_delta):
    global Missiles_List
    x=0
    y=0
    if InputSet[pygame.K_d] or InputSet[pygame.K_RIGHT]:
        x+=1
    if InputSet[pygame.K_a] or InputSet[pygame.K_LEFT]:
        x-=1
    if InputSet[pygame.K_w] or InputSet[pygame.K_UP]:
        y-=1
    if InputSet[pygame.K_s] or InputSet[pygame.K_DOWN]:
        y+=1
    
    PLAYER_SPACE_SHIP.changeColor(InputSet[pygame.K_LSHIFT])
    miss = PLAYER_SPACE_SHIP.shoot(InputSet[pygame.K_SPACE], True)
    if(miss != None):
        Missiles_List.append(miss)
    
    miss = PLAYER_SPACE_SHIP.shoot(MouseInputSet[0],False)
    if(miss != None):
        Missiles_List.append(miss)
    PLAYER_SPACE_SHIP.movePlayer(x,y,SCREEN_OF_GAME, current_delta)   

def movementEnemies(current_delta):
    SEM.movementEnemies(SCREEN_OF_GAME, current_delta)

def checkIfEnemyShoot(current_delta):
    global Missiles_List
    Missiles_List+=SEM.checkShootEnemies(current_delta)
    
def movementMissiles(current_delta):
    global Missiles_List
    for mis in Missiles_List:
        if mis.Update(SCREEN_OF_GAME, current_delta) == 1:
            Missiles_List.remove(mis)
    
def checkCollisions():
    global Missiles_List
    global CurrentGameState
    if PLAYER_SPACE_SHIP.checkCollision(Missiles_List):
        GFM.saveScore(PLAYER_SPACE_SHIP.player)
        CurrentGameState = GameStates.DEATH
    SEM.checkCollisionEnemies(Missiles_List)

####################################### M A C R O  -  F U N C T I O N S #######################################
def UpdateMenu():
    global isPlaying
    global CurrentGameState
    result = PRINCIPAL_MENU.Update(pygame.mouse.get_pressed())        # Update the Menu
    if result != None:
        if result:
            CurrentGameState = GameStates.LVL_1
        else:
            isPlaying = False
            CurrentGameState = GameStates.EXIT

def UpadteLVL1(delta):
    SCREEN_OF_GAME.fill((0,0,0))        # Update the fill
    inputPlayer(pygame.key.get_pressed(), pygame.mouse.get_pressed(), delta)        # Update the player position and Input
    SEM.spawnCheckExec(delta)
    movementEnemies(delta)      # Update enemies position
    checkIfEnemyShoot(delta)    # Check if enemies must shoot and add missiles
    movementMissiles(delta)     # Update all missiles position
    checkCollisions()      # Check if missiles position collapse with Player/Enemies

def UpdateDeathMenu():
    global CurrentGameState
    global isPlaying
    SCREEN_OF_GAME.fill((0,0,0))
    res = DEATH_MENU.Update(pygame.mouse.get_pressed())
    if res != None:
        if res:
            PLAYER_SPACE_SHIP.resetValues()
            SEM.resetValues()
            Missiles_List.clear()
            CurrentGameState = GameStates.PRINCIPAL_MENU
        else:
            isPlaying = False
            CurrentGameState = GameStates.EXIT

####################################### M A I N  L O O P  G A M E #######################################
while isPlaying:

    delta=clock.tick(60)    # The time in ms between frames
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            isPlaying = False     # We search for the event of pygame to break the while Loop
    
    if CurrentGameState == GameStates.PRINCIPAL_MENU:
        UpdateMenu()
    elif CurrentGameState == GameStates.LVL_1:
        UpadteLVL1(delta)
    elif CurrentGameState == GameStates.BOSS: 
        pass
    elif CurrentGameState == GameStates.DEATH:
        UpdateDeathMenu()
    elif CurrentGameState == GameStates.EXIT:
        pass
    

    
    pygame.display.flip()

####################################### E X I T #######################################
pygame.quit()