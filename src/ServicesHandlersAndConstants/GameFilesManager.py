####################################### I M P O R T S #######################################
import os
from Objects.Player import Player

class GameFilesManager():
    def __init__(self):
        self.path=None

    def getActualProjectFolder(self):
        if self.path == None:
            arrayFullPath = __file__.split('/')
            
            path = ''
            for folder in arrayFullPath:
                if 'src' in folder:
                    path = path.rstrip()
                    break
                path+=folder+'/'
            
            self.path = path
        
        return self.path
    
    def saveScore ( self, player : Player ) :
        if not os.path.exists( self.path +'/SavedGames' ):
            os.mkdir( self.path + '/SavedGames' )
        
        f = open(self.path+'SavedGames/F0.csv', 'a')
        f.write(player.name+","+str(player.score)+'\n')
        
        try:
            f = open(self.path+'SavedGames/F0.csv', 'r')
        
        except:
            print(self.path+'SavedGames/F0.csv')