####################################### C O N S T A N T S #######################################
class   GameConstants():
    ###################### Window Constants ######################
    TITLE_OF_GAME="Projecte Pol Gonzalo Pygame A1"
    IMAGE_FOLDER="Resources/Images/icon.png"
    ###################### Menu Constants ######################
    PATH_BACKGROUND_MENU = "Resources/Images/UI/Menu/BackgroundMenu.png"
    PATH_TITLE_MENU = "Resources/Images/UI/Menu/Title.png"
    PATH_START_BUTT_MENU = "Resources/Images/UI/Menu/StartButton.png"
    PATH_START_BUTT_HOVER_MENU = "Resources/Images/UI/Menu/StartButtonHover.png"
    PATH_START_BUTT_PRESS_MENU = "Resources/Images/UI/Menu/StartButtonPressed.png"
    PATH_EXIT_BUTT_MENU = "Resources/Images/UI/Menu/ExitButton.png"
    PATH_EXIT_BUTT_HOVER_MENU = "Resources/Images/UI/Menu/ExitButtonHover.png"
    PATH_EXIT_BUTT_PRESS_MENU = "Resources/Images/UI/Menu/ExitButtonPressed.png"
    ###################### Main Game Constants ######################
    ################## Missiles ##################
    PATH_MISSILE_B="Resources/Images/Projectiles/Missile_B.png"
    PATH_MISSILE_R="Resources/Images/Projectiles/Missile_R.png"
    PATH_MISSILE_B_E="Resources/Images/Projectiles/Missile_B_Enemy.png"
    PATH_MISSILE_R_E="Resources/Images/Projectiles/Missile_R_Enemy.png"
    PATH_MEGAMISSILE="Resources/Images/Projectiles/MegaMissile.png"
    ################## Spaceship ##################
    PATH_PLAYER_SPACESHIP_B="Resources/Images/Player/Player_0.png"
    PATH_PLAYER_SPACESHIP_R="Resources/Images/Player/Player_1.png"
    PATH_ENEMY_SPACESHIP_B="Resources/Images/Enemy/Enemy_0.png"
    PATH_ENEMY_SPACESHIP_R="Resources/Images/Enemy/Enemy_1.png"
    ################## Sizes ##################
    P_I_W = 50.0
    P_I_H = 35.5
    M_W = 11.0
    M_H = 14.0
    MM_S = 429
    E_I_W = 60.0
    E_I_H = 42.0
    ###################### Death Menu Constants ######################
    PATH_TITLE_DEATH_MENU = "Resources/Images/UI/DeathMenu/Title.png"
    PATH_BACKMENU_BUTT_DEATH = "Resources/Images/UI/DeathMenu/BackMenu.png"
    PATH_BACKMENU_BUTT_HOVER_DEATH = "Resources/Images/UI/DeathMenu/BackMenuHover.png"
    PATH_BACKMENU_BUTT_PRESS_DEATH = "Resources/Images/UI/DeathMenu/BackMenuPressed.png"
